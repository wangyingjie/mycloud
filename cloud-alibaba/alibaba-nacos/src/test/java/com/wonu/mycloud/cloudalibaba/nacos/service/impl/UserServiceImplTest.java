package com.wonu.mycloud.cloudalibaba.nacos.service.impl;

import com.wonu.mycloud.cloudalibaba.nacos.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserServiceImplTest
 * @date 2023/12/7 11:54
 * @description
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceImplTest {

    @Resource
    private UserService userService;

    @Test
    public void findAll() {

        System.out.println(userService.findAll());

    }

    @Test
    public void test() {

        int n = 10;
        char c = 'a';
        String s = String.valueOf(c);
//        System.out.println(s);
        String s1 = Character.toString(c);
        System.out.println(s1);
        String s2 = "1";
        int i = Integer.parseInt(s2);
        String s3 = "3.23";
        double d = Double.parseDouble(s3);
        String s4 = "hello,java";
        char c1 = s4.charAt(0);
        char[] chars = s4.toCharArray();
        Integer integer = new Integer(10);
        int i1 = integer.intValue();
    }

}