package com.wonu.mycloud.cloudalibaba.nacos.service;



import com.wonu.mycloud.domain.po.User;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserService
 * @date 2023/12/7 11:47
 * @description
 */
public interface UserService {

    List<User> findAll();

    User findById(Long id);

    boolean modifyUser(User user);

    boolean addUser(User user);

    boolean removeUserById(Long id);
}
