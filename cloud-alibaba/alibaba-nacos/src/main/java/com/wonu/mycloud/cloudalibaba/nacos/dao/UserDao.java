package com.wonu.mycloud.cloudalibaba.nacos.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.wonu.mycloud.domain.po.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserDao
 * @date 2023/12/7 11:47
 * @description
 */
@Mapper
public interface UserDao extends BaseMapper<User> {
}
