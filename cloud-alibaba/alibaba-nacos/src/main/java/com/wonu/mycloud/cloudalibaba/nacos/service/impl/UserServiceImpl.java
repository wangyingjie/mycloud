package com.wonu.mycloud.cloudalibaba.nacos.service.impl;

import com.wonu.mycloud.domain.po.User;
import com.wonu.mycloud.cloudalibaba.nacos.dao.UserDao;

import com.wonu.mycloud.cloudalibaba.nacos.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserServiceImpl
 * @date 2023/12/7 11:50
 * @description
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;

    /**
     * @author nameless
     * @date 2023/12/7 19:39
     * @param
     * @return List<User>
     * @description 查询所有用户
     */
    @Override
    public List<User> findAll() {

        return userDao.selectList(null);
    }

    /**
     * @author nameless
     * @date 2023/12/7 19:39
     * @param id  Long型
     * @return User
     * @description 通过id查询用户
     */
    @Override
    public User findById(Long id) {

        return  userDao.selectById(id);
    }

    /**
     * @author nameless
     * @date 2023/12/7 19:39
     * @param user User
     * @return boolean
     * @description 修改用户
     */
    @Override
    public boolean modifyUser(User user) {

        return userDao.updateById(user) > 0;
    }

    /**
     * @author nameless
     * @date 2023/12/7 19:39
     * @param user User
     * @return boolean
     * @description 添加用户
     */
    @Override
    public boolean addUser(User user) {

        return userDao.insert(user) > 0;
    }

    /**
     * @author nameless
     * @date 2023/12/7 19:40
     * @param id Long
     * @return boolean
     * @description 通过id移除用户
     */
    @Override
    public boolean removeUserById(Long id) {
        return userDao.deleteById(id) > 0;
    }
}