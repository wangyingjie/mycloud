package com.wonu.mycloud.cloudalibaba.nacos.controller;


import com.wonu.mycloud.common.result.HttpResp;
import com.wonu.mycloud.domain.po.User;
import com.wonu.mycloud.cloudalibaba.nacos.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserController
 * @date 2023/12/7 19:41
 * @description   用户信息管理
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Resource
    private UserService userService;


    @GetMapping("findUsers")
    public HttpResp<List<User>> findUsers(){
        List<User> users = userService.findAll();
        return HttpResp.success(users);
    }
}