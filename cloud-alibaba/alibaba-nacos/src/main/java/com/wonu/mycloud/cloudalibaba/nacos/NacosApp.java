package com.wonu.mycloud.cloudalibaba.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author nameless
 * @version 1.0
 * @ClassName NacosApp
 * @date 2023/12/6 17:22
 * @description
 */
@SpringBootApplication
/*说明该启动类作为注册中心服务*/
@EnableDiscoveryClient
public class NacosApp {
    public static void main(String[] args) {
        SpringApplication.run(NacosApp.class,args);
    }
}