package com.wonu.mycloud.cloudalibaba.nacos.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author nameless
 * @version 1.0
 * @ClassName NacosController
 * @date 2023/12/6 17:24
 * @description
 */
@RestController
/* 使得 nacos远程的配置发生更改时进行实时读取，无需重启服务器*/
@RefreshScope
@RequestMapping("/api/nacos")
@Slf4j
public class NacosController {

    @Value("${mybatis-plus.configuration.log-impl}")
    private String name;

    @GetMapping("/test")
    public String test() {

        log.debug("从nacos中读取的配置name:{}", name);
        return name;
    }
}