package com.wonu.mycloud.user.service.impl;

import com.wonu.mycloud.common.result.HttpResp;
import com.wonu.mycloud.payCenter.api.PayCenterFeign;
import com.wonu.mycloud.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserServiceImpl
 * @date 2023/12/11 0:00
 * @description
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Resource
    private PayCenterFeign payCenterFeign;

    @Resource
    private TaskScheduler taskScheduler;

    /**
     * @author nameless
     * @date 2023/12/11 1:25
     * @param money
     * @param business
     * @param token
     * @return CompletableFuture<HttpResp>
     * @description 异步执行前开启一个定时任务
     */
    @Override
    @Async("myAsyncExecutor") //使用自定义的线程池配置
    public CompletableFuture<HttpResp>  pay(double money, long business, String token) {

        payScheduleTask(business,token);

        log.debug("异步任务开始执行，远程调用pay方法");
        HttpResp resp = payCenterFeign.pay(money, business, token);

        return  CompletableFuture.completedFuture(resp);
    }

    /**
     * @author nameless
     * @date 2023/12/11 1:29
     * @param business
     * @param token
     * @return void
     * @description 循环查询支付结果
     */
    private void payScheduleTask(long business,String token) {

        log.debug("开启定时查询任务");
        long timeMillis = System.currentTimeMillis();
        taskScheduler.schedule(()->{

            HttpResp status = payCenterFeign.getStatus(business, token);
            if(status.getCode() == 500) {
                payScheduleTask(business,token);
            }else {

                //向前端传递信息
                System.out.println("久等了，已支付完成。");
            }

        },new Date(timeMillis + TimeUnit.SECONDS.toMillis(10)));
    }
}