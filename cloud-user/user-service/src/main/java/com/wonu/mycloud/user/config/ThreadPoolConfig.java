package com.wonu.mycloud.user.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ThreadPoolConfig
 * @date 2023/12/10 23:39
 * @description  SpringBoot开启异步调用的线程池配置  @EnableAsync 表示开启异步
 */
@Configuration
@EnableAsync
@Slf4j
public class ThreadPoolConfig {


    @Bean("myAsyncExecutor")
    public ThreadPoolTaskExecutor threadPoolExecutor() {

        log.debug("线程池初始化");
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setMaxPoolSize(8);
        executor.setKeepAliveSeconds(120);
        executor.setQueueCapacity(20);
        // 设置线程名前缀
        executor.setThreadNamePrefix("task-");
        // 设置拒绝策略，调用者模式
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 等待所有任务结束后，再关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        return executor;
    }
}