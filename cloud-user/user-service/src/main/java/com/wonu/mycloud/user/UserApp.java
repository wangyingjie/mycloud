package com.wonu.mycloud.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserApp
 * @date 2023/12/8 14:16
 * @description
 */
@EnableScheduling //开启定时任务
@SpringBootApplication
@EnableDiscoveryClient
/*  需要进行指定包加载feign接口， 不然Spring无法找到*/
@EnableFeignClients(basePackages = {"com.wonu.mycloud.payCenter.api"})
public class UserApp {
    public static void main(String[] args) {
        SpringApplication.run(UserApp.class,args);
    }
}