package com.wonu.mycloud.user.service;

import com.wonu.mycloud.common.result.HttpResp;

import java.util.concurrent.CompletableFuture;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserService
 * @date 2023/12/10 23:59
 * @description
 */
public interface UserService {


    CompletableFuture<HttpResp> pay(double money, long business, String token);
}
