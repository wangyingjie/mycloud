package com.wonu.mycloud.user.controller;

import com.wonu.mycloud.common.result.HttpResp;
import com.wonu.mycloud.domain.dto.AccountDto;
import com.wonu.mycloud.payCenter.api.PayCenterFeign;
import com.wonu.mycloud.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserController
 * @date 2023/12/8 14:22
 * @description
 */
@RestController
@Api(tags = "用户相关接口")
@RequestMapping("/api/user")
public class UserController {

    //RestTemplate形式的远程调用 搭配loadBalancer进行负载均衡
    /*@Resource
    private RestTemplate restTemplate;

    @GetMapping("getAccountByNumber")
    public HttpResp getAccountByNumber(long phone) {

        String url = "http://myCloud-payCenter"+"/api/account/findByNumber/{num}";

        ResponseEntity<HttpResp> entity = restTemplate.getForEntity(url, HttpResp.class, phone);

        return entity.getBody();

    }*/

    @Resource
    private PayCenterFeign payCenterFeign;




    @Resource
    private UserService userService;


    @GetMapping("getAccountByNumber")
    public HttpResp getAccountByNumber(long phone) {

        return payCenterFeign.findByNumber(phone);

    }

    @ApiOperation(value = "getBalance", notes = "获取用户余额")
    @GetMapping("getBalance")
    public HttpResp getBalance(HttpServletRequest request) {


        //将当前请求对象直接传递给远程对象?并不能直接传递连接对象，但可以传递头信息
        // 方式一：使用@RequestHander标注在feign接口的参数上，调用方将需要设置的头信息提取封装并传递给feign接口
        String token = request.getHeader("token");
        /*直接传给远程服务端，进行token检测*/
        /*if(!StringUtils.hasText(token)) {
            throw new BmsException(500,"token为空");
        }*/

        return payCenterFeign.getBlance(token);

    }


    /**
     * @author nameless
     * @date 2023/12/9 14:14
     * @param accountDto
     * @param token
     * @return HttpResp
     * @description 远程调用登录，获取的token一般返回给前端，下次请求头携带token过来
     *
     */
    @ApiOperation(value = "loginPay", notes = "支付登录")
    @PostMapping("loginPay")
    public HttpResp loginPay(AccountDto accountDto, String token) {

        return payCenterFeign.checkAccount(accountDto, token);
    }


    /**
     * @author nameless
     * @date 2023/12/9 18:27
     * @param money
     * @return HttpResp
     * @description   1.为了避免远程支付调用出现耗时情况，需要使用线程池进行异步调用，如何向前端返回两种信息?
     *               - 这就牵扯到后端主动向前端传递数据，webSocket 当然异步任务结束顺便发送短信或邮件也许。（未实现）
     *               2. 异步方法迟迟拿不到结果，怎么办？
     *               进行异步方法前，先向mq发送一个延迟队列，时间是容忍阻塞的最大时间，如果到那是还未放回消息，则消费者方法执行
     *               查询交易记录状态的方法，或者开启一个定时任务
     *
     */
    @PostMapping("pay")
    public HttpResp pay(double money, long business,HttpServletRequest request) {

        String token = request.getHeader("token");
        CompletableFuture<HttpResp> futureResult = userService.pay(money, business, token);

        HttpResp httpResp = HttpResp.success(200, "支付完成,结果稍后通知",null);

        // 获取返回值的get()方法是同步的，因而这下面的代码是同步了
        try {

            HttpResp asyncResult = futureResult.get();
            //执行websocket发送消息给前端。。

        } catch (InterruptedException  | ExecutionException e) {
            throw new RuntimeException(e);
        }

        return httpResp;
    }

}