package com.wonu.mycloud.user.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RestTemplateConfig
 * @date 2023/12/8 14:23
 * @description
 */
@Configuration
@Slf4j
public class RestTemplateConfig {

    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        log.debug("RestTemplate 初始化");
        return new RestTemplate();
    }
}