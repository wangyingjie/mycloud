package com.wonu.mycloud.common.exception;

import lombok.Getter;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ErrorCode
 * @date 2023/12/9 14:59
 * @description
 */
@Getter
public enum ErrorCode {
    TOKEN_IS_NULL(500, "token 为空"),
    TOKEN_ERROR(501, "token 非法"),;
    int code;
    String message;

    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
