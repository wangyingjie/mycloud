package com.wonu.mycloud.common.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nameless
 * @version 1.0
 * @ClassName HttpResp
 * @date 2023/12/7 19:46
 * @description  同一返回类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpResp <T> {

    private int code;
    private String message;
    private T data;

    public static <T> HttpResp<T> success(T data) {
        return new HttpResp<>(200, "success", data);
    }

    public static <T> HttpResp<T> success(int code, String message, T data) {
        return new HttpResp<>(code, message, data);
    }

    public static <T> HttpResp<T> error() {
        return new HttpResp<>(500, "fail", null);
    }

    public static <T> HttpResp<T> error(int code, String message) {
        return new HttpResp<>(code, message, null);
    }
}