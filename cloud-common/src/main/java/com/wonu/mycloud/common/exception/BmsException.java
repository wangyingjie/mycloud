package com.wonu.mycloud.common.exception;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BmsException
 * @date 2023/12/9 14:57
 * @description
 */
@Data
@NoArgsConstructor
public class BmsException extends RuntimeException{

    private int code;
    private String message;

    public BmsException(int code, String message) {
        this.code = code;
        this.message = message;
    }
}