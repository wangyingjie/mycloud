package com.wonu.mycloud.common.exception;

import com.wonu.mycloud.common.result.HttpResp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BmsExecptionHander
 * @date 2023/12/9 14:57
 * @description  业务异常处理类
 */
@RestControllerAdvice
@Slf4j
public class BmsExceptionHandle {

    @ExceptionHandler(BmsException.class)
    public HttpResp handleException(BmsException e) {
        log.debug("捕获了业务异常BmsException");
        return HttpResp.error(e.getCode(), e.getMessage());
    }


    //如果既有父类异常处理又有子类异常处理，会选取最匹配的处理方法执行
   /* @ExceptionHandler(TransactionException.class)
    public HttpResp handleException(TransactionException e) {
        return HttpResp.error(e.getCode(), e.getMessage());
    }*/



}