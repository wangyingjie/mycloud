package com.wonu.mycloud.common.result;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RespCode
 * @date 2023/12/7 19:46
 * @description  返回结构体的枚举类
 */
public enum RespCode {
    SUCCESS(200, "success"),
    FAIL(400, "fail"),
    ERROR(500, "error");
    private int code;
    private String msg;

    RespCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
