package com.wonu.mycloud.common.exception;

import lombok.Getter;

/**
 * @author nameless
 * @version 1.0
 * @ClassName TransactionException
 * @date 2023/12/9 17:37
 * @description
 */

public class TransactionException extends BmsException{
    public TransactionException(int code, String message) {
        super(code, message);
    }

    @Override
    public int getCode() {
        return super.getCode();
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}