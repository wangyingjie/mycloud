package com.wonu.mycloud.domain.po;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Translation)实体类
 *
 * @author makejava
 * @since 2023-12-08 11:50:02
 */
@Data
@TableName("t_translation")
public class Translation {
    
    @TableId(value = "translation_id")
    private Long translationId;
    /**
     * 发起账户
     */    
    @TableField("initiate_account")
    private Long initiateAccount;
    /**
     * 接收账户
     */    
    @TableField("receive_account")
    private Long receiveAccount;
    /**
     * 交易金额
     */    
    @TableField("trans_money")
    private BigDecimal transMoney;
    /**
     * 交易状态
     */    
    @TableField("status")
    private Integer status;
    /**
     * 交易时间
     */    
    @TableField("trans_time")
    private Date transTime;
        
    @TableField("create_time")
    private Date createTime;
        
    @TableField("update_time")
    private Date updateTime;
        
    @TableField("create_by")
    private String createBy;

}
