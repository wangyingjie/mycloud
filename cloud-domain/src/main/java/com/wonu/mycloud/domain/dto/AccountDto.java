package com.wonu.mycloud.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AccountDto
 * @date 2023/12/8 20:07
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {

    private Long number;
    private String password;
//    private String token;

}