package com.wonu.mycloud.domain.po;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Account)实体类
 *
 * @author makejava
 * @since 2023-12-08 11:48:38
 */
@Data
@TableName("t_account")
public class Account {
    
    @TableId(value = "account_id")
    private Long accountId;
    /**
     * 账户
     */    
    @TableField("number")
    private Long number;
    /**
     * 密码
     */    
    @TableField("password")
    private String password;
    /**
     * 余额
     */    
    @TableField("balance")
    private BigDecimal balance;
    /**
     * 状态：
        0：正常
        1：警告
        -1：封禁
     */    
    @TableField("status")
    private Integer status;
        
    @TableField("create_time")
    private Date createTime;
        
    @TableField("update_time")
    private Date updateTime;
        
    @TableField("create_by")
    private String createBy;

}
