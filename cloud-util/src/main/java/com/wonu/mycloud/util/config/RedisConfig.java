package com.wonu.mycloud.util.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RedisConfig
 * @date 2023/12/8 20:31
 * @description
 */
@Configuration
@Slf4j
public class RedisConfig {


    @Bean
    public RedisTemplate<String,Object> redisTemplate(RedisConnectionFactory factory) {

        //new redisTemplate 模板
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        // 将factory 连接工作对象设置进模板
        redisTemplate.setConnectionFactory(factory);
        // String类型进行K-V的序列化设置
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        // Hash类型数据的K-V序列化设置
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());

        log.debug("RedisTemplate 初始化成功");
        return redisTemplate;

    }
}