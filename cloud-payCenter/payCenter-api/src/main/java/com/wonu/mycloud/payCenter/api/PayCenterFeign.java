package com.wonu.mycloud.payCenter.api;

import com.wonu.mycloud.common.result.HttpResp;
import com.wonu.mycloud.domain.dto.AccountDto;
import com.wonu.mycloud.domain.po.Account;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author nameless
 * @version 1.0
 * @ClassName PayCenterApi
 * @date 2023/12/8 17:10
 * @description   公开的OpenFeign 接口
 */
/* 指定注册在注册中心中的微服务名称，便于后续进行负载均衡 */
@FeignClient(value = "myCloud-payCenter")
public interface PayCenterFeign {

    /* 若服务端为路径参数形式通过 @PathVariable 进行绑定，若为查询参数形式，则使用@RequestParam()进行绑定
    * 使用Feign接口，传递的参数，必须加上注解，实体类*/
    @GetMapping("/api/account/findByNumber")
    HttpResp<Account> findByNumber(@RequestParam("num") long num);

    @PostMapping("/api/account/checkAccount")
    HttpResp<String> checkAccount(@RequestBody AccountDto accountDto,@RequestParam(value = "token",required = false) String token);

    @GetMapping("/api/account/getBalance")
    HttpResp getBlance(@RequestHeader String token);


    @PutMapping("/api/account/pay")
    HttpResp pay(@RequestParam("money") double money,@RequestParam("business") long business,@RequestHeader String token);


    @GetMapping("/api/trans/getStatus")
    HttpResp getStatus(@RequestParam("business") long business, @RequestHeader String token);
}
