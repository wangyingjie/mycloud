package com.wonu.mycloud.payCenter.payService.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.mycloud.domain.po.Account;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AccountDao
 * @date 2023/12/8 12:07
 * @description
 */
@Mapper
public interface AccountDao extends BaseMapper<Account> {
}
