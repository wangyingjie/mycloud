package com.wonu.mycloud.payCenter.payService.bmsConst;

/**
 * @author nameless
 * @version 1.0
 * @ClassName Const
 * @date 2023/12/9 16:29
 * @description   业务常量定义接口
 */
public interface Const {

    String TOKEN_REDIS_KEY_PREFIX = "account-token:";
}