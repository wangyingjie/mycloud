package com.wonu.mycloud.payCenter.payService.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.mycloud.domain.po.Translation;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName TranslationDao
 * @date 2023/12/9 17:28
 * @description
 */
@Mapper
public interface TranslationDao extends BaseMapper<Translation> {
}
