package com.wonu.mycloud.payCenter.payService.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wonu.mycloud.domain.po.Translation;
import com.wonu.mycloud.payCenter.payService.dao.TranslationDao;
import com.wonu.mycloud.payCenter.payService.service.TranslationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author nameless
 * @version 1.0
 * @ClassName TranslationServiceImpl
 * @date 2023/12/9 17:27
 * @description
 */
@Service
public class TranslationServiceImpl implements TranslationService {


    @Resource
    private TranslationDao translationDao;

    /**
     * @author nameless
     * @date 2023/12/9 17:29
     * @param translation
     * @return boolean
     * @description 添加交易记录
     */
    @Override
    public boolean addTranslation(Translation translation) {


        return translationDao.insert(translation) > 0;
    }

    /**
     * @author nameless
     * @date 2023/12/11 1:07
     * @param number
     * @param business
     * @return Translation
     * @description    注意：此方法不完善，仅通过双方无法唯一确定是当前的订单
     */
    @Override
    public Translation getTransByNumber(long number, long business) {

        LambdaQueryWrapper<Translation> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Translation::getInitiateAccount,number)
                        .eq(Translation::getReceiveAccount,business);

        return translationDao.selectOne(queryWrapper);
    }
}