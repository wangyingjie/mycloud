package com.wonu.mycloud.payCenter.payService.service.impl;

import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wonu.mycloud.common.exception.BmsException;
import com.wonu.mycloud.domain.dto.AccountDto;
import com.wonu.mycloud.domain.po.Account;
import com.wonu.mycloud.domain.po.Translation;
import com.wonu.mycloud.payCenter.payService.bmsConst.Const;
import com.wonu.mycloud.payCenter.payService.dao.AccountDao;
import com.wonu.mycloud.payCenter.payService.service.AccountService;
import com.wonu.mycloud.payCenter.payService.service.TranslationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AccountServiceImpl
 * @date 2023/12/8 12:09
 * @description
 */
@Service
@Slf4j
public class AccountServiceImpl implements AccountService {


    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    private AccountDao accountDao;

    @Resource
    private TranslationService translationService;


    @Override
    public List<Account> findAll() {

        return accountDao.selectList(null);
    }

    @Override
    public Account findByNumber(Long num) {

        LambdaQueryWrapper<Account> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Account::getNumber, num);

        return accountDao.selectOne(lambdaQueryWrapper);
    }

    @Override
    public boolean addAccount(Account account) {


        return accountDao.insert(account) > 0;
    }

    /**
     * @param accountDto
     * @return Account
     * @author nameless
     * @date 2023/12/8 20:13
     * @description 通过用户名和密码进行校验账户
     */
    @Override
    public Account findByNumberAndPassword(AccountDto accountDto) {

        LambdaQueryWrapper<Account> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Account::getNumber, accountDto.getNumber())
                .eq(Account::getPassword, accountDto.getPassword());

        return accountDao.selectOne(lambdaQueryWrapper);

    }

    /**
     * @param account
     * @return String
     * @author nameless
     * @date 2023/12/8 20:22
     * @description 根据账户类生成token, 有效时间30分钟
     */
    @Override
    public String generateToken(Account account) {

        String salt = String.valueOf(account.getAccountId());

        String jwt = JWT.create().withClaim("number", account.getNumber())
                .withClaim("status", account.getStatus())
                .sign(Algorithm.HMAC256(salt));

        redisTemplate.opsForValue().set(Const.TOKEN_REDIS_KEY_PREFIX + jwt, salt, 120, TimeUnit.MINUTES);
        // 怎么为hash 中的某一个key进行单独设置过期时间? 答案是不行 ,

        return jwt;
    }

    @Override
    public boolean checkToken(String token) {

        Object value = redisTemplate.opsForValue().get(Const.TOKEN_REDIS_KEY_PREFIX + token);
        return value != null;
    }

    @Override
    @Transactional
    public void pay(long number, double money, long business) {


        try {
            log.debug("支付执行中...");
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        Account account = findByNumber(number);
        BigDecimal balance = account.getBalance();
        BigDecimal cost = BigDecimal.valueOf(money);

        BigDecimal subtract = balance.subtract(cost);
        if (subtract.compareTo(BigDecimal.ZERO) < 0) {
            throw new BmsException(503, "余额不足");
        }
        try {
            // 余额充足则更新余额，创建交易记录
            account.setBalance(subtract);
            accountDao.updateById(account);

            Translation translation = new Translation();
            translation.setInitiateAccount(account.getNumber());
            translation.setReceiveAccount(business);
            translation.setTransMoney(cost);
            translation.setStatus(1);  //1表示完成
            translation.setTransTime(new Date());
            translation.setCreateTime(new Date());
            translation.setCreateBy("admin");

            translationService.addTranslation(translation);
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }

    }

}