package com.wonu.mycloud.payCenter.payService.service;

import com.wonu.mycloud.domain.dto.AccountDto;
import com.wonu.mycloud.domain.po.Account;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AccountService
 * @date 2023/12/8 12:08
 * @description
 */
public interface AccountService {

    List<Account> findAll();
    Account findByNumber(Long num);
    boolean addAccount(Account account);
    Account findByNumberAndPassword(AccountDto accountDto);

    String generateToken(Account account);

    boolean checkToken(String token);

    void pay(long number, double money, long business);
}
