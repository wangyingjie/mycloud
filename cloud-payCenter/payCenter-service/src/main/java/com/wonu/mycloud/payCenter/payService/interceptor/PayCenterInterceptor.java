package com.wonu.mycloud.payCenter.payService.interceptor;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.wonu.mycloud.common.exception.BmsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * @author nameless
 * @version 1.0
 * @ClassName PayCenterIntercepter
 * @date 2023/12/9 15:15
 * @description  支付中心的拦截器，用于判断是否携带了token
 */
@Slf4j
public class PayCenterInterceptor implements HandlerInterceptor {

    @Resource
    private RedisTemplate<String,Object> redisTemplate;


    /*重写PreHandle方法，表示请求执行前进行拦截处理*/
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        log.debug("进入PayCenter 拦截器");
        String jwt = request.getHeader("token");

        if(!StringUtils.hasText(jwt)) {
            throw new BmsException(500,"当前请求未携带Pay-token");
        }
        Object value = redisTemplate.opsForValue().get("account-token:" + jwt);
        if(Objects.isNull(value)){
            throw new BmsException(500,"当前token不存在于redis，非法");
        }
        String salt = (String) value;

        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(salt)).build();

        try{
            verifier.verify(jwt);
        }catch (SignatureVerificationException | JWTDecodeException e){
            throw new BmsException(500,"pay-token 被篡改");
        }

        //放行请求
        return true;
    }
}