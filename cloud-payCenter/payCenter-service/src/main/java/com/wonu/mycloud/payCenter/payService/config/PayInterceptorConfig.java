package com.wonu.mycloud.payCenter.payService.config;

import com.wonu.mycloud.payCenter.payService.interceptor.PayCenterInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author nameless
 * @version 1.0
 * @ClassName PayInterceptorConfig
 * @date 2023/12/9 16:14
 * @description   请求拦截器
 */
@Configuration
@Slf4j
public class PayInterceptorConfig implements WebMvcConfigurer {

    @Bean
    public PayCenterInterceptor payCenterInterceptor() {
        log.debug("PayCenterInterceptor 初始化");
        return new PayCenterInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(payCenterInterceptor())
                .excludePathPatterns("/api/account/checkAccount");
    }
}