package com.wonu.mycloud.payCenter.payService.controller;

import com.auth0.jwt.JWT;
import com.wonu.mycloud.common.result.HttpResp;
import com.wonu.mycloud.domain.po.Translation;
import com.wonu.mycloud.payCenter.payService.service.TranslationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @author nameless
 * @version 1.0
 * @ClassName TranslationController
 * @date 2023/12/11 1:02
 * @description   交易记录控制类
 */
@RestController
@RequestMapping("/api/trans")
public class TranslationController {

    @Resource
    private TranslationService translationService;

    @GetMapping("getStatus")
    public HttpResp getStatus(@RequestParam("business") long business, HttpServletRequest request) {

        String token = request.getHeader("token");
        Long number = JWT.decode(token).getClaim("number").asLong();
        Translation trans = translationService.getTransByNumber(number, business);
        if(Objects.nonNull(trans) && trans.getStatus() == 1){
            return HttpResp.success(200,"支付完成",null);
        }

        return HttpResp.error(500,"未支付完成");
    }
}