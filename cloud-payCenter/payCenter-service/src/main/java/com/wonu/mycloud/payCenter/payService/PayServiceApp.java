package com.wonu.mycloud.payCenter.payService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author nameless
 * @version 1.0
 * @ClassName PayServiceApp
 * @date 2023/12/8 11:59
 * @description  扫描common是为了进行全局捕获异常
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
@ComponentScan(basePackages = {"com.wonu.mycloud.payCenter.payService", "com.wonu.mycloud.util","com.wonu.mycloud.common"})
public class PayServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(PayServiceApp.class,args);
    }
}