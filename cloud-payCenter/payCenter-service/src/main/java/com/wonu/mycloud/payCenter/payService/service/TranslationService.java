package com.wonu.mycloud.payCenter.payService.service;

import com.wonu.mycloud.domain.po.Translation;

/**
 * @author nameless
 * @version 1.0
 * @ClassName TranslationService
 * @date 2023/12/9 17:27
 * @description  交易信息
 */
public interface TranslationService {

    boolean addTranslation(Translation translation);

    Translation getTransByNumber(long number, long business);
}
