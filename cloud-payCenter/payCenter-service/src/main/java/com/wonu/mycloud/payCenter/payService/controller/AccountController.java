package com.wonu.mycloud.payCenter.payService.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Verification;
import com.wonu.mycloud.common.exception.BmsException;
import com.wonu.mycloud.common.result.HttpResp;
import com.wonu.mycloud.domain.dto.AccountDto;
import com.wonu.mycloud.domain.po.Account;
import com.wonu.mycloud.payCenter.payService.service.AccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Objects;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AccountController
 * @date 2023/12/8 12:12
 * @description
 */
@RestController
@RequestMapping("/api/account")
@Api(tags = "账户相关接口")
@Slf4j
public class AccountController {


    @Resource
    private AccountService accountService;

    @Resource
    private RedisTemplate<String,Object>  redisTemplate;

    @Resource
    private HttpServletRequest request;
    @Value("${server.port}")
    private String port;

    @GetMapping("findAll")
    public HttpResp<List<Account>> findAll() {

        return HttpResp.success(accountService.findAll());
    }

    @PostMapping("addAccount")
    public HttpResp addAccount(Account account) {

        boolean b = accountService.addAccount(account);
        return b ? HttpResp.success(null): HttpResp.error(500, "添加失败");
    }

    @GetMapping("findByNumber")
    public HttpResp<Account> findByNumber(@RequestParam("num") long num)  {


        String address = request.getHeader("Host") + ":" + port;

        return HttpResp.success(200, address,accountService.findByNumber(num));
    }


    /**
     * @author nameless
     * @date 2023/12/8 21:17
     * @param accountDto
     * @return HttpResp
     * @description 校验支付账户，需要进行等幂性处理
     */
    @ApiOperation(value = "checkAccount", notes = "校验支付账户")
    @PostMapping("checkAccount")
    public HttpResp checkAccount(@RequestBody AccountDto accountDto, @RequestParam(value = "token",required = false)String token)  {

        if(StringUtils.hasText(token)) {
            if(accountService.checkToken(token)){
                return HttpResp.success(200, "请勿重复登录",null);
            }
        }


        Account account = accountService.findByNumberAndPassword(accountDto);
        if(account == null) {
            return HttpResp.error(400, "账号或密码错误");
        }else{
            // 若存在则生成token
            String jwt = accountService.generateToken(account);


            return HttpResp.success(200, "校验成功", jwt);
        }
    }


    /**
     * @author nameless
     * @date 2023/12/9 16:45
     * @param
     * @return HttpResp
     * @description  查询余额，从token中获取账户
     */
    @GetMapping("getBalance")
    public HttpResp getBalance() {

        String token = request.getHeader("token");
        log.debug("payCenter接收到的token：{}",token);
        long number = JWT.decode(token).getClaim("number").asLong();
        Account account = accountService.findByNumber(number);

        return HttpResp.success(200,"查询余额成功，你的当前余额：",account.getBalance());

    }


    /**
     * @author nameless
     * @date 2023/12/10 23:05
     * @param money
     * @param business
     * @return HttpResp
     * @description 支付业务
     */
    @PutMapping("/pay")
    public HttpResp pay(@RequestParam("money")double money,@RequestParam("business")long business) {
        String token = request.getHeader("token");
        log.debug("payCenter接收到的token：{}",token);
        Long number = JWT.decode(token).getClaim("number").asLong();
        // 模拟5秒的业务流程

        accountService.pay(number,money,business);

        return HttpResp.success(200, "支付成功",null);

    }



}